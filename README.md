Role: docker-gitlab-runner
=========
A brief description of the role :

This role create `docker` gitlab-runners containers for cicd pipelines.

Requirements :pushpin:
------------
Any pre-requisites that may not be covered by Ansible itself : 

- python3

Role Variables :wrench:
--------------

```yml
#default/main.yml
runner_docker_image: gitlab/gitlab-runner:v14.9.1
cicd_default_docker_image: docker:20.10.14-dind
runner_url: https://gitlab.com/
runner_registration_token: "{{ vault_runner_registration_token }}"
runner_tags: ""
runner_qty: 2
```


Dependencies :paperclip:
------------
A list of other roles should go here and any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles :

- **role** : [docker-install](https://github.com/enzo-cora/ansible-role-docker-install)

Example Playbook :clapper:
----------------

```yml 
- name : Init gitlab runners
  gather_facts: false
  hosts:
    - all
  roles:
    - docker-install
    - gitlab-runner
```
